Command line instructions


Git global setup

git config --global user.name "zhouhailin"
git config --global user.email "hailin.zhou@foxmail.com"

Create a new repository

git clone https://gitlab.com/zhouhailin/hello.git
cd hello
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/zhouhailin/hello.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/zhouhailin/hello.git
git push -u origin --all
git push -u origin --tags
